print ("Starting")

import os
import sys
import time

from kubernetes import client, config
import yaml
#probably already imported by kub api
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

NAMESPACE="default"
JOB_NAME="pi" #from job.yaml


def getPodsForJob(aKubeClient):
    print("getting job pods")
    aPod=None
    aJobStatus=getJobStatus(aKubeClient)
    aSelectorFromJob = aJobStatus.spec.selector
    #print("Selector retrieve from job: ", aSelectorFromJob)
    if "controller-uid" in aSelectorFromJob.match_labels:
        aContolerUid=aSelectorFromJob.match_labels["controller-uid"]
        #print("Match controlerUID found in job selector labels: ",aContolerUid)
        aPodApi = client.CoreV1Api(aKubeClient)
        aTextLabel = "controller-uid=" + aContolerUid
        aPodForJob = aPodApi.list_pod_for_all_namespaces(label_selector=aTextLabel)
        if len(aPodForJob.items)==1:
            aPod = aPodForJob.items[0]
            #print("Pod found: ",aPod)
        else:
            print("Error with the number of pod found. Here are the match results: ",aPodForJob)
    else:
        print("No controlerUIDlabel found in job selector labels: ",aSelectorFromJob.match_labels)
    return aPod

def getLogs(aKubeClient, iPod):
    print("getting job logs")
    aPodApi = client.CoreV1Api(aKubeClient)
    aLogs = aPodApi.read_namespaced_pod_log(iPod.metadata.name,NAMESPACE)
    #print("Logs: ",aLogs)
    return aLogs

def createJob(aKubeClient):
    print("Starting job")
    with open("Job.yaml") as aJobTemplateStream:
        aRawJobTemplate = aJobTemplateStream.read()
        aJobAsYaml = yaml.load(aRawJobTemplate)
    prettyReq = 'true' # str | If 'true', then the output is pretty printed. (optional)
    aBatchApi = client.BatchV1Api(aKubeClient)
    aApiResponse = aBatchApi.create_namespaced_job(NAMESPACE, aJobAsYaml, pretty=prettyReq)
    #print("aApiResponse: ", aApiResponse)

def deleteJob(aKubeClient):
    print("Cleaning up the job")
    aBatchApi = client.BatchV1Api(aKubeClient)
    aDeleteOptions = client.V1DeleteOptions() # V1DeleteOptions | 
    prettyReq = 'true' # str | If 'true', then the output is pretty printed. (optional)
    grace_period_seconds = 56 # int | The duration in seconds before the object should be deleted. Value must be non-negative integer. The value zero indicates delete immediately. If this value is nil, the default grace period for the specified type will be used. Defaults to a per object value if not specified. zero means delete immediately. (optional)
    orphan_dependents = True # bool | Deprecated: please use the PropagationPolicy, this field will be deprecated in 1.7. Should the dependent objects be orphaned. If true/false, the \"orphan\" finalizer will be added to/removed from the object's finalizers list. Either this field or PropagationPolicy may be set, but not both. (optional)
    propagation_policy = 'propagation_policy_example' # str | Whether and how garbage collection will be performed. Either this field or OrphanDependents may be set, but not both. The default policy is decided by the existing finalizer set in the metadata.finalizers and the resource-specific default policy. Acceptable values are: 'Orphan' - orphan the dependents; 'Background' - allow the garbage collector to delete the dependents in the background; 'Foreground' - a cascading policy that deletes all dependents in the foreground. (optional)
    aApiResponse = aBatchApi.delete_namespaced_job(JOB_NAME, NAMESPACE, aDeleteOptions, pretty=prettyReq, grace_period_seconds=grace_period_seconds, orphan_dependents=orphan_dependents, propagation_policy=propagation_policy)
    #print("aApiResponse: ", aApiResponse)

def getJobStatus(aKubeClient):
    print("Checking job status")
    aBatchApi = client.BatchV1Api(aKubeClient)
    prettyReq = 'true' # str | If 'true', then the output is pretty printed. (optional)
    aApiResponse = aBatchApi.read_namespaced_job_status(JOB_NAME, NAMESPACE, pretty=prettyReq)
    #print("aApiResponse: ", aApiResponse)
    return aApiResponse

def waitUntilJobEnd(aKubeClient):
    aJobSucceed = True
    aJobStatus=getJobStatus(aKubeClient)
    while (aJobStatus.status.active==1):
        print("Job is still running. Sleep 1s")
        time.sleep(1)
        aJobStatus=getJobStatus(aKubeClient)
    #print("Job is over with status: ", aJobStatus)
    print("Job is over")
    if aJobStatus.status.succeeded==1:
        aJobSucceed = True
    else:
        aJobSucceed = False
    return aJobSucceed

def createKubeClient(iTokenName, iEndPoint):
    # Define the barer token we are going to use to authenticate.
    # See here to create the token:
    # https://kubernetes.io/docs/tasks/access-application-cluster/access-cluster/
    aToken = os.getenv(iTokenName)
    if not aToken:
        print("Kubernetes token not provided.")
        sys.exit(1)

    # Create a configuration object
    aConfiguration = client.Configuration()

    # Specify the endpoint of your Kube cluster
    aConfiguration.host = iEndPoint

    # Security part.
    # In this simple example we are not going to verify the SSL certificate of
    # the remote cluster (for simplicity reason)
    aConfiguration.verify_ssl = False
    # Nevertheless if you want to do it you can with these 2 parameters
    # configuration.verify_ssl=True
    # ssl_ca_cert is the filepath to the file that contains the certificate.
    # configuration.ssl_ca_cert="certificate"

    aConfiguration.api_key = {"authorization": "Bearer " + aToken}

    # Create a ApiClient with our config
    aApiClient = client.ApiClient(aConfiguration)

    return aApiClient

def main():
    # https://docs.docker.com/docker-for-windows/networking/#known-limitations-use-cases-and-workarounds to connect on host from container on windows
    aKubeClient = createKubeClient("KUBE_TOKEN", "https://host.docker.internal:6445")
    createJob(aKubeClient)
    aJobSucceess = waitUntilJobEnd(aKubeClient)
    #Try to get the logs
    aPodJob = getPodsForJob(aKubeClient)
    if aPodJob:
        aJobLogs = getLogs(aKubeClient,aPodJob)
        print("Job is over without error. Here are the logs: ",aJobLogs)
    else:
        print("There was an error when getting the logs of the pod/job.")
    #it is up to the user to clean up: https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/
    deleteJob(aKubeClient)
    if not aJobSucceess:
        sys.exit("Job failed.")

if __name__ == '__main__':
    main()

print ("Ending")