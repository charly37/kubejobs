# KubeJobs

This demo is made on a windows 10 computer. It shows how to interact with a Kube cluster in python and start a simple job on it and wait for the job to end and get its status/logs. The tricky part is to get the logs since the job object do not directly contains the info and thus, we need to get the pod associated with the job and get the pod logs. 

## Kube (Server)

I use the Kube functionality of Docker for windows.

Create a SA
PS C:\Users\charl> kubectl create serviceaccount jobdemo 

Give access to the SA
PS C:\Users\charl> kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --serviceaccount default:jobdemo 

Get token
PS C:\Users\charl> kubectl get secret jobdemo-token-jk59q -o json 

Save the token (after you decode it from base 64)

## Python (Client)

Build the docker image
PS C:\Code\kubejobs> docker build -t quicktest . 

run it (and mount the repo folder in the conatiner)
PS C:\Code\kubejobs> docker run -it -v C:\Code\kubejobs:/mountfolder quicktest 

Export the Kube token (the one saved before)
[root@54a8362da7d1 mountfolder]# export KUBE_TOKEN=eyJhbGciOiJSUzI1NiIsImtpZCI...Es5howDOSTWqzl9JQAFH_xCpo1Q 

Start the script
[root@54a8362da7d1 mountfolder]# python3.6 kubeJobsDemo.py 
